package org.nsu.fit.tm_backend.manager;

import org.slf4j.Logger;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Globals;


import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект класса Customer. Ограничения:
     * Аргумент 'customer' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * balance - должно быть равно 0 перед отправкой базу данных.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        if (customer == null) {
            throw new IllegalArgumentException("Argument 'customer' is null.");
        }

        if (customer.pass == null) {
            throw new IllegalArgumentException("Field 'customer.pass' is null.");
        }

        if (customer.pass.length() < 6 || customer.pass.length() > 12) {
            throw new IllegalArgumentException("Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        }

        if (customer.pass.equalsIgnoreCase("123qwe")) {
            throw new IllegalArgumentException("Password is very easy.");
        }


        // Лабораторная 2: добавить код который бы проверял, что нет customer'а c таким же login (email'ом).
        // Попробовать добавить другие ограничения, посмотреть как быстро растет кодовая база тестов.


        if (customer.pass.equalsIgnoreCase("1q2w3e")) {
            throw new IllegalArgumentException("Password is very easy.");
        }

        if (customer.login == null) {
            throw new IllegalArgumentException("Field 'customer.login' is null.");
        }

        if (customer.firstName == null) {
            throw new IllegalArgumentException("Field 'customer.firstName' is null.");
        }

        if (customer.lastName == null) {
            throw new IllegalArgumentException("Field 'customer.lastName' is null.");
        }

        if (customer.firstName.length() < 2 || customer.firstName.length() > 12) {
            throw new IllegalArgumentException("FirstName's length should be more 2 symbols and less 12 symbols.");
        }

        if (customer.lastName.length() < 2 || customer.lastName.length() > 12) {
            throw new IllegalArgumentException("LastName's length should be more 2 symbols and less 12 symbols.");
        }

        if(customer.firstName.contains(" ")) {
            throw new IllegalArgumentException("FirstName contains space.");
        }

        if(customer.lastName.contains(" ")) {
            throw new IllegalArgumentException("LastName contains space.");
        }

        if(!customer.firstName.matches("[a-zA-Z]+")) {
            throw new IllegalArgumentException("FirstName contains not letters.");
        }

        if(!customer.lastName.matches("[a-zA-Z]+")) {
            throw new IllegalArgumentException("LastName contains not letters.");
        }

        if(!Character.isUpperCase(customer.firstName.charAt(0))) {
            throw  new IllegalArgumentException("First letter of firstName is not uppercase.");
        }

        if(!Character.isUpperCase(customer.lastName.charAt(0))) {
            throw  new IllegalArgumentException("First letter of lastName is not uppercase.");
        }

        if (!customer.firstName.substring(1).equals(customer.firstName.substring(1).toLowerCase())) {
            throw new IllegalArgumentException("FirstName have upper case after first symbol.");
        }

        if (!customer.lastName.substring(1).equals(customer.lastName.substring(1).toLowerCase())) {
            throw new IllegalArgumentException("LastName have upper case after first symbol.");
        }

        if(!customer.login.matches(".+@.+\\..+")){
            throw new IllegalArgumentException("Login is not email.");
        }

        if(lookupCustomer(customer.login) != null) {
            throw new IllegalArgumentException("Customer with same login exists.");
        }

        if(customer.pass.contains(customer.login)){
            throw  new IllegalArgumentException("Password contains login.");
        }

        if(customer.pass.contains(customer.firstName)){
            throw  new IllegalArgumentException("Password contains firstName.");
        }

        if(customer.pass.contains(customer.lastName)){
            throw  new IllegalArgumentException("Password contains lastName.");
        }

        if(customer.balance != 0){
            throw new IllegalArgumentException("Balance is not 0.");
        }

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список customer'ов.
     */
    public List<CustomerPojo> getCustomers() {
        return dbService.getCustomers();
    }

    public CustomerPojo getCustomer(UUID customerId) {
        return dbService.getCustomer(customerId);
    }

    public CustomerPojo lookupCustomer(String login) {
        return dbService.getCustomers().stream()
                .filter(x -> x.login.equals(login))
                .findFirst()
                .orElse(null);
    }

    public ContactPojo me(AuthenticatedUserDetails authenticatedUserDetails) {
        ContactPojo contactPojo = new ContactPojo();

        if (authenticatedUserDetails.isAdmin()) {
            contactPojo.login = Globals.ADMIN_LOGIN;
            contactPojo.pass = null;
            return contactPojo;
        }

        // Лабораторная 2: обратите внимание что вернули данных больше чем надо...
        // т.е. getCustomerByLogin честно возвратит все что есть в базе данных по этому customer'у.
        // необходимо написать такой unit тест, который бы отлавливал данное поведение.
        CustomerPojo customerByLogin = dbService.getCustomerByLogin(authenticatedUserDetails.getName());
        if (customerByLogin != null) {
            customerByLogin.pass = null;
        }
        return customerByLogin;
    }

    public void deleteCustomer(UUID id) {
        dbService.deleteCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу переданное значение, которое должно быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(TopUpBalancePojo topUpBalancePojo) {
        CustomerPojo customerPojo = dbService.getCustomer(topUpBalancePojo.customerId);

        if(topUpBalancePojo.money <= 0){
            throw new IllegalArgumentException("Money in topUpBalancePojo is negative.");
        }

        customerPojo.balance += topUpBalancePojo.money;

        dbService.editCustomer(customerPojo);

        return customerPojo;
    }
}

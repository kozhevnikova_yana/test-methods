package org.nsu.fit.tm_backend.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Authority;
import org.nsu.fit.tm_backend.shared.Globals;
import org.slf4j.Logger;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

// Лабораторная 2: покрыть юнит тестами класс CustomerManager на 100%.
class CustomerManagerTest {
    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;
    @BeforeEach
    void init() {
        // Создаем mock объекты.
        dbService = mock(IDBService.class);
        logger = mock(Logger.class);

        // Создаем класс, методы которого будем тестировать,
        // и передаем ему наши mock объекты.
        customerManager = new CustomerManager(dbService, logger);
    }

    /*тесты, которые были*/
    @Test
    void testCreateCustomer() {
        // настраиваем mock.
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@example.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...verify(dbService, times(0)).getCustomers();
    }

    // Как не надо писать тест...
    @Test
    void testCreateCustomerWithNullArgument_Wrong() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            assertEquals("Argument 'customer' is null.", ex.getMessage());
        }
    }

    @Test
    void testCreateCustomerWithNullArgument_Right() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals("Argument 'customer' is null.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerWithNullPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = null;
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.pass' is null.", exception.getMessage());
    }

    /*-------------------------------------------*/

    @Test
    void  testCreateCustomerWithNullLogin() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = null;
        createCustomerInput.pass = "password";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.login' is null.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerWithNullFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = null;
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "password";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.firstName' is null.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerWithNullLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = null;
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "password";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.lastName' is null.", exception.getMessage());
    }


    @Test
    void  testCreateCustomerFirstNameHasSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Kfk dlf";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName contains space.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerLastNameHasSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Kfkdlf";
        createCustomerInput.lastName = "Wic k";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName contains space.", exception.getMessage());
    }


    @Test
    void  testCreateCustomerFirstNameLenLessThenLeftBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "K";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerFirstNameLenMoreThenRightBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerLastNameLenLessThemLeftBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "W";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void  testCreateCustomerLastNameLenMoreThenRightBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wickkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName's length should be more or equal 2 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithEasyPassword1() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "123qwe";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithEasyPassword2() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "1q2w3e";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception.getMessage());
    }

    @Test
    void testCreateCustomerPasswordLenLessThemLeftBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "av";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void testCreateCustomerPasswordLenMoreThenRightBorder() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "abdedfffffffffffffffffg";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithFirstLetterIsNotInUpperCaseInFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "john";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("First letter of firstName is not uppercase.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithFirstLetterIsNotInUpperCaseInLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("First letter of lastName is not uppercase.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithUpperCaseInFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "JohnQ";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName have upper case after first symbol.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithUpperCaseInLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "WiCk";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName have upper case after first symbol.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithSameLogin() {
        CustomerPojo customer1 = new CustomerPojo();
        customer1.firstName = "Johnq";
        customer1.lastName = "Wickq";
        customer1.login = "john_wick@example.com";
        customer1.pass = "Baba_Jaga";
        customer1.balance = 0;

        CustomerPojo customer2 = new CustomerPojo();
        customer2.firstName = "Johnk";
        customer2.lastName = "Wickk";
        customer2.login = "john_wick@example.com";
        customer2.pass = "Baba_Jaga";
        customer2.balance = 0;

        List<CustomerPojo> customerPojos = new ArrayList<>();
        customerPojos.add(customer1);

        when(dbService.getCustomers()).thenReturn(customerPojos);
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(customer2));
        assertEquals("Customer with same login exists.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNotCorrectLogin() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@ex";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Login is not email.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNotZeroBalance() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 5;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Balance is not 0.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNumbersInFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John1";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "password";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("FirstName contains not letters.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNumbersInLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick1";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "password";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("LastName contains not letters.", exception.getMessage());
    }


    @Test
    void testCreateCustomerPasswordContainsLogin() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "jo@mail.com";
        createCustomerInput.pass = "jo@mail.com";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password contains login.", exception.getMessage());
    }

    @Test
    void testCreateCustomerPasswordContainsFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "mister@example.com";
        createCustomerInput.pass = "fsfsJohn";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password contains firstName.", exception.getMessage());
    }


    @Test
    void testCreateCustomerPasswordContainsLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "mister@example.com";
        createCustomerInput.pass = "sfsfWick";
        createCustomerInput.balance = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password contains lastName.", exception.getMessage());
    }

    /*------------------------------------------------*/


    @Test
    void testGetCustomers() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        List<CustomerPojo> customerPojos = new ArrayList<>();
        customerPojos.add(createCustomerInput);
        when(dbService.getCustomers()).thenReturn(customerPojos);

        List<CustomerPojo> customers = customerManager.getCustomers();

        assertNotNull(customers);
        assertEquals(createCustomerInput, customers.get(0));

        verify(dbService, times(1)).getCustomers();
    }


    @Test
    void testGetCustomer() {
        CustomerPojo customer = new CustomerPojo();
        customer.id = UUID.randomUUID();
        customer.firstName = "John";
        customer.lastName = "Wick";
        customer.login = "john_wick@example.com";
        customer.pass = "Baba_Jaga";
        customer.balance = 0;
        when(dbService.getCustomer(customer.id)).thenReturn(customer);

        assertEquals(customer, customerManager.getCustomer(customer.id));

        verify(dbService, times(1)).getCustomer(customer.id);

    }

    @Test
    void testLookupCustomerNotNull() {
        CustomerPojo customerPojo = new CustomerPojo();
        String login = "john_wick@example.com";

        customerPojo.id = UUID.randomUUID();
        customerPojo.firstName = "John";
        customerPojo.lastName = "Wick";
        customerPojo.login = "john_wick@example.com";
        customerPojo.pass = "Baba_Jaga";
        customerPojo.balance = 0;

        ArrayList<CustomerPojo> list = new ArrayList<>();
        list.add(customerPojo);

        when(dbService.getCustomers()).thenReturn(list);
        CustomerPojo actual = customerManager.lookupCustomer(login);
        assertEquals(customerPojo, actual);

        verify(dbService, times(1)).getCustomers();

    }

    @Test
    void testLookupCustomerNull() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        assertNull(customerManager.lookupCustomer(createCustomerInput.login));

        verify(dbService, times(1)).getCustomers();

    }

    @Test
    void testMeAdmin() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "admin";
        createCustomerInput.pass = "Baba_Jaga";
        Set<String> roles = new HashSet<>();
        roles.add(Authority.ADMIN_ROLE);
        AuthenticatedUserDetails authenticatedUserDetails = new AuthenticatedUserDetails(createCustomerInput.id.toString(), createCustomerInput.login, roles);

        ContactPojo contactPojo = customerManager.me(authenticatedUserDetails);
        assertEquals(contactPojo.login, Globals.ADMIN_LOGIN);
        assertNull(contactPojo.pass);
    }


    @Test
    void testMeCustomer() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        Set<String> roles = new HashSet<>();
        roles.add(Authority.CUSTOMER_ROLE);

        AuthenticatedUserDetails authenticatedUserDetails = new AuthenticatedUserDetails(createCustomerInput.id.toString(), createCustomerInput.login, roles);

        when(dbService.getCustomerByLogin(authenticatedUserDetails.getName())).thenReturn(createCustomerInput);
        ContactPojo contactPojo = customerManager.me(authenticatedUserDetails);

        assertNull(contactPojo.pass);
    }

    @Test
    void testDelete() {
        UUID id = UUID.randomUUID();
        customerManager.deleteCustomer(id);
        verify(dbService, times(1)).deleteCustomer(id);
    }

    @Test
    void testTopUpBalance() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick1";
        createCustomerInput.login = "john_wick1@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        when(dbService.getCustomer(createCustomerInput.id)).thenReturn(createCustomerInput);

        TopUpBalancePojo topUpBalancePojoInput = new TopUpBalancePojo();
        topUpBalancePojoInput.customerId = createCustomerInput.id;
        topUpBalancePojoInput.money = 13;

        CustomerPojo customer = customerManager.topUpBalance(topUpBalancePojoInput);

        verify(dbService, times(1)).getCustomer(createCustomerInput.id);
        verify(dbService, times(1)).editCustomer(customer);
        assertEquals(13, customer.balance);
    }

    @Test
    void testTopUpBalanceNegative() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick1";
        createCustomerInput.login = "john_wick1@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        when(dbService.getCustomer(createCustomerInput.id)).thenReturn(createCustomerInput);

        TopUpBalancePojo topUpBalancePojoInput = new TopUpBalancePojo();
        topUpBalancePojoInput.customerId = createCustomerInput.id;
        topUpBalancePojoInput.money = -13;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.topUpBalance(topUpBalancePojoInput));
        assertEquals("Money in topUpBalancePojo is negative.", exception.getMessage());
    }
}

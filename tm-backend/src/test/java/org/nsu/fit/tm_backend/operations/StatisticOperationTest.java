package org.nsu.fit.tm_backend.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.IDBService;

import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.SubscriptionPojo;
import org.nsu.fit.tm_backend.manager.CustomerManager;
import org.nsu.fit.tm_backend.manager.SubscriptionManager;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public class StatisticOperationTest {
    // Лабораторная 2: покрыть юнит тестами класс StatisticOperation на 100%.

    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;
    private SubscriptionManager subscriptionManager;
    private  List<UUID> customerIds;

    private StatisticOperation statisticOperation;
    @BeforeEach
    void init() {
        customerManager = mock(CustomerManager.class);
        subscriptionManager = mock(SubscriptionManager.class);
        customerIds = new ArrayList<>();

        statisticOperation = new StatisticOperation(customerManager, subscriptionManager, customerIds);
    }


    @Test
    void testCreateStatisticOperationWithNullCustomerManager() {
        List<UUID> uuids = new ArrayList<>();
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> new StatisticOperation(null, subscriptionManager, uuids));
        assertEquals("customerManager", exception.getMessage());
    }

    @Test
    void testCreateStatisticOperationWithNullSubscriptionManager() {
        List<UUID> uuids = new ArrayList<>();
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> new StatisticOperation(customerManager, null, uuids));
        assertEquals("subscriptionManager", exception.getMessage());
    }

    @Test
    void testCreateStatisticOperationWithNullUUIDS() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> new StatisticOperation(customerManager, subscriptionManager, null));
        assertEquals("customerIds", exception.getMessage());
    }

    @Test
    void testExecute() {
        List<UUID> uuids = new ArrayList<>();


        CustomerPojo customerPojo = new CustomerPojo();
        customerPojo.id = UUID.randomUUID();
        customerPojo.firstName = "John";
        customerPojo.lastName = "Wick";
        customerPojo.login = "john_wick@example.com";
        customerPojo.pass = "Baba_Jaga";
        customerPojo.balance = 13;
        uuids.add(customerPojo.id);


        CustomerPojo customerPojo1 = new CustomerPojo();
        customerPojo1.id = UUID.randomUUID();
        customerPojo1.firstName = "John";
        customerPojo1.lastName = "Wick";
        customerPojo1.login = "john_wick@example.com";
        customerPojo1.pass = "Baba_Jaga";
        customerPojo1.balance = 50;
        uuids.add(customerPojo1.id);

        SubscriptionPojo subscriptionPojo = new SubscriptionPojo();
        subscriptionPojo.id = UUID.randomUUID();
        subscriptionPojo.customerId = customerPojo.id;
        subscriptionPojo.planFee = 51;
        subscriptionPojo.planDetails = "details";
        subscriptionPojo.planName = "planName";
        subscriptionPojo.planId = UUID.randomUUID();

        SubscriptionPojo subscriptionPojo1 = new SubscriptionPojo();
        subscriptionPojo1.id = UUID.randomUUID();
        subscriptionPojo1.customerId = customerPojo.id;
        subscriptionPojo1.planFee = 52;
        subscriptionPojo1.planDetails = "details1";
        subscriptionPojo1.planName = "planName1";
        subscriptionPojo1.planId = UUID.randomUUID();

        SubscriptionPojo subscriptionPojo2 = new SubscriptionPojo();
        subscriptionPojo2.id = UUID.randomUUID();
        subscriptionPojo2.customerId = customerPojo1.id;
        subscriptionPojo2.planFee = 53;
        subscriptionPojo2.planDetails = "details2";
        subscriptionPojo2.planName = "planName2";
        subscriptionPojo2.planId = UUID.randomUUID();


        List<SubscriptionPojo> subscriptionPojos1 = new ArrayList<>();
        subscriptionPojos1.add(subscriptionPojo);
        subscriptionPojos1.add(subscriptionPojo1);

        List<SubscriptionPojo> subscriptionPojos2 = new ArrayList<>();
        subscriptionPojos2.add(subscriptionPojo2);

        when(customerManager.getCustomer(customerPojo.id)).thenReturn(customerPojo);
        when(customerManager.getCustomer(customerPojo1.id)).thenReturn(customerPojo1);
        when(subscriptionManager.getSubscriptions(customerPojo.id)).thenReturn(subscriptionPojos1);
        when(subscriptionManager.getSubscriptions(customerPojo1.id)).thenReturn(subscriptionPojos2);

        statisticOperation = new StatisticOperation(customerManager, subscriptionManager, uuids);

        StatisticOperation.StatisticOperationResult statisticOperationResult = statisticOperation.Execute();

        assertEquals(63, statisticOperationResult.overallBalance);
        assertEquals(156, statisticOperationResult.overallFee);

        verify(customerManager, times(2)).getCustomer(any());
        verify(subscriptionManager, times(2)).getSubscriptions(any());
        verify(customerManager, times(1)).getCustomer(customerPojo.id);
        verify(customerManager, times(1)).getCustomer(customerPojo1.id);
        verify(subscriptionManager, times(1)).getSubscriptions(customerPojo.id);
        verify(subscriptionManager, times(1)).getSubscriptions(customerPojo1.id);
        verifyNoMoreInteractions(customerManager, subscriptionManager);
    }
}

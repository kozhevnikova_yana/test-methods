package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;

import org.nsu.fit.services.rest.data.HealthCheckPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HealthCheckTest {
    private RestClient restClient;

    @BeforeClass
    private void beforeClass() {
        restClient = new RestClient();
    }

    @Test(description = "Test health check")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Health check feature.")
    public void healthCheck() {
        HealthCheckPojo healthCheckPojo = restClient.healthCheck();
        Assert.assertEquals(healthCheckPojo.status, "OK");
    }

}

package org.nsu.fit.tests.ui;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.tests.ui.screen.AdminScreen;
import org.nsu.fit.tests.ui.screen.CustomerScreen;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Locale;

public class TopUpBalanceTest {
    private Browser browser = null;
    private final static Faker faker = new Faker(new Locale("en-US"));

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test(description = "Top up balance via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Top up balance feature")
    public void topUpBalanceTest() {

        CustomerPojo customerPojo = new CustomerPojo();
        Name name = faker.name();
        customerPojo.firstName = name.firstName();
        customerPojo.lastName = name.lastName();
        customerPojo.login = faker.internet().emailAddress();
        customerPojo.pass = faker.internet().password(6, 12);

        AdminScreen screen = new LoginScreen(browser)
                .loginAsAdmin()
                .createCustomer()
                .fillEmail(customerPojo.login)
                .fillPassword(customerPojo.pass)
                .fillFirstName(customerPojo.firstName)
                .fillLastName(customerPojo.lastName)
                .clickSubmit();

        CustomerScreen customerScreen = screen.logout()
                .loginAsCustomer(customerPojo.login, customerPojo.pass);

        customerScreen
                .topUpBalance()
                .fillMoney(10)
                .clickSubmit();

        Assert.assertEquals(10, customerScreen.getBalance());

    }

    @AfterClass
    public void afterClass() {
        if (browser != null) {
            browser.close();
        }
    }
}

package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

public class PlanTest {

    private RestClient restClient;
    private AccountTokenPojo adminToken;

    @BeforeClass
    private void auth() {
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
    }


    boolean checkEquals(PlanPojo pojo1, PlanPojo pojo2) {
        return pojo1.id.equals(pojo2.id) &&
                pojo1.name.equals(pojo2.name) &&
                pojo1.details.equals(pojo2.details);
    }

    @Test(description = "Create plan")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature.")
    public void createPlan() {
        PlanPojo planPojo = restClient.createAutoGeneratePlan(adminToken);
        List<PlanPojo> plans = restClient.getPlans(adminToken);

        Optional<PlanPojo> planPojo1 = plans.stream()
                .filter(x -> checkEquals(x, planPojo)).findFirst();

        Assert.assertTrue(planPojo1.isPresent());
    }

    @Test(description = "Delete plan")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature.")
    public void deletePlan() {
        PlanPojo planPojo = restClient.createAutoGeneratePlan(adminToken);
        restClient.deletePlane(planPojo.id, adminToken);
        Assert.assertFalse(restClient.getPlans(adminToken).contains(planPojo));

        List<PlanPojo> plans = restClient.getPlans(adminToken);
        Optional<PlanPojo> planPojo1 = plans.stream()
                .filter(x -> checkEquals(x, planPojo)).findFirst();

        Assert.assertFalse(planPojo1.isPresent());
    }
}

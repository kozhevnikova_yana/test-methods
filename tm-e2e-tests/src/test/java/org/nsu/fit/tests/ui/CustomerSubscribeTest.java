package org.nsu.fit.tests.ui;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.nsu.fit.tests.ui.screen.AdminScreen;
import org.nsu.fit.tests.ui.screen.CustomerScreen;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Locale;

public class CustomerSubscribeTest {
    private Browser browser = null;
    private String login;
    private String pass;
    private RestClient restClient;
    private final static Faker faker = new Faker(new Locale("en-US"));
    private CustomerScreen customerScreen;
    private AccountTokenPojo customerAccauntToken;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
        restClient = new RestClient();
        CustomerPojo customerPojo = new CustomerPojo();
        Name name = faker.name();
        customerPojo.firstName = name.firstName();
        customerPojo.lastName = name.lastName();
        customerPojo.login = faker.internet().emailAddress();
        customerPojo.pass = faker.internet().password(6, 12);
        PlanPojo planPojo = new PlanPojo();
        planPojo.name = faker.lorem().characters(2, 128);
        planPojo.details = faker.lorem().characters(10, 1024);
        planPojo.fee = faker.number().numberBetween(0, 5000);
        AdminScreen screen = new LoginScreen(browser)
                .loginAsAdmin()
                .createCustomer()
                .fillEmail(customerPojo.login)
                .fillPassword(customerPojo.pass)
                .fillFirstName(customerPojo.firstName)
                .fillLastName(customerPojo.lastName)
                .clickSubmit()
                .createPlan()
                .fillName(planPojo.name)
                .fillDetails(planPojo.details)
                .fillFee(planPojo.fee)
                .clickSubmit();
        screen.logout();
        login = customerPojo.login;
        pass = customerPojo.pass;
    }

    @Test(description = "Subscribe to plan via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Subscribe feature")
    public void subscribeToFirstPlanTest() {
        customerScreen = new LoginScreen(browser)
                .loginAsCustomer(login, pass)
                .subscribeToFirst();
        customerAccauntToken = restClient.authenticate(login, pass);
        List<PlanPojo> subscriptions = restClient.getAvailablePlans(customerAccauntToken);

        Assert.assertNotNull(subscriptions);
        Assert.assertNotEquals(0, subscriptions.size());
    }

    @Test(description = "Subscribe to plan via UI.", dependsOnMethods = {"subscribeToFirstPlanTest"})
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Subscribe feature")
    public void unsubscribe() {
        customerScreen.unsubscribeToFirst();

        List<PlanPojo> subscriptions = restClient.getAvailablePlans(customerAccauntToken);

        Assert.assertNotNull(subscriptions);
        Assert.assertNotEquals(0, subscriptions.size());
    }

    @AfterClass
    public void afterClass() {
        if (browser != null) {
            browser.close();
        }
    }
}
